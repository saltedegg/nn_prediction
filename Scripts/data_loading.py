"""

Processing the Dataframe

"""
from watstocksdk import Watstock

"""

Imports

"""
from drop_features import get_drop_columns
import pandas as pd
import numpy as np

"""

Functions

"""
watstock = Watstock(access_token="aWnkBW0teY")


def get_data(datasource, version, ticker, day):
    df = watstock.read.data(
        datasource=datasource,
        version=version,
        ticker=ticker,
        resolution="1D"
    )
    # backtest
    if day < -1:
        df = df[:day + 1]
    return df


def get_data_starting_at_date(df, date):
    if date:
        df = df.loc[date:]
    return df


def get_df_X(df):
    drop_features = get_drop_columns()
    df_X = df.drop(drop_features, axis=1)
    return df_X


def get_df_y(df, horizon):
    return df[get_y_columns(horizon)]


def get_y_columns(horizon, multi_label=False):
    """
    Get target columns

    :param horizon: prediction horizon
    :param multi_label: whether we predict multiple labels
    :return: column names for target
    """
    index = horizon + 1
    return 'close_pct_%d' % index


def make_X_3d(train_X, memory_length):
    train_X_3d = {}
    dates = train_X[memory_length + 1:].index
    for date in dates:
        df = train_X.loc[:date][-memory_length:]
        df.index = range(len(df.index))
        train_X_3d[date] = df
    # multiindexing, df.loc[date] returns data with history for that date
    train_X_3d = pd.concat(train_X_3d)
    return train_X_3d


def convert_multiindex_to_3d_array(df):
    dates = df.index.get_level_values(0).unique()
    shape = df.loc[dates[0]].shape
    return np.array([np.array(df.loc[date]).reshape(shape[0], shape[1]) for date in dates])
