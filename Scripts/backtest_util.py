import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_absolute_error, mean_squared_error
import os


def _get_root_dir_name(ticker):
    path = 'backtest_result/%s/' % ticker
    if not os.path.exists(path):
        os.makedirs(path)
    return path


def _create_csv_columns_name(num):
    result = []
    for i in range(num):
        result.append('%ddays_ahead' % (i+1))
    return result


def _varied_length_list_to_array(the_list,filled_value=np.nan):
    max_len = len(sorted(the_list, key=len, reverse=True)[0])
    if len(the_list) == 1:
        list_filled = np.array(the_list)
    else:
        list_filled = np.array([xi + [filled_value] * (max_len - len(xi)) for xi in the_list])
    return list_filled


def _check_list_type(the_list):
    if len(the_list) == 0:
        return None
    list_len = len(the_list)
    for i in range(list_len):
        if len(the_list[i]) > 0:
            return type(the_list[i][0])
    return float


def write_price_pic(ticker, predicted_prices, actual_prices, dates=None):
    assert len(actual_prices) == len(predicted_prices)
    predicted_prices = _varied_length_list_to_array(predicted_prices)
    actual_prices = _varied_length_list_to_array(actual_prices)
    if dates is not None:
        assert len(predicted_prices[0] == len(dates))
    for i in range(0, len(actual_prices)):
        plt.plot(predicted_prices[i])
        plt.plot(actual_prices[i])
        if dates is not None:
            plt.xticks(range(len(dates)), dates, rotation='vertical')
        column_names = ['predicted', 'actual_price']
        plt.legend(column_names)
        plt.savefig(_get_root_dir_name(ticker) + '%ddays_ahead.png'%(i+1))
        plt.close()


def write_csv(ticker, predicted, real_prices, start_dates=None, result_dates=None):
    assert (len(predicted) == len(real_prices))
    predicted = _varied_length_list_to_array(predicted)
    real_prices = _varied_length_list_to_array(real_prices)
    result_dates = _varied_length_list_to_array(result_dates)
    columns = ['%ddays_ahead_predicted' % (i + 1) for i in range(len(predicted))]
    columns.extend(['%ddays_ahead_actual' % (i + 1) for i in range(len(predicted))])
    if result_dates is not None:
        columns.extend(['%ddays_ahead_date' % (i + 1) for i in range(len(predicted))])
        result = np.concatenate((predicted.T, real_prices.T, result_dates.T), axis=1)
    else:
        result = np.concatenate((predicted.T, real_prices.T), axis=1)
    if start_dates is None:
        df = pd.DataFrame(result, columns=columns)
    else:
        df = pd.DataFrame(result, columns=columns, index=start_dates)
    file_name = _get_root_dir_name(ticker) + '%s_result.csv' % ticker
    df = df.dropna(axis=1, how='all')
    df.to_csv(file_name)


def calculate_error(ticker, predicted_results, actual_prices, dates=None):
    assert len(actual_prices) == len(predicted_results)
    predicted_results = _varied_length_list_to_array(predicted_results, filled_value=0)
    actual_prices = _varied_length_list_to_array(actual_prices, filled_value=0)

    # a[:, ~np.isnan(a).any(axis=0)]
    result = []
    for i in range(0, len(actual_prices)):
        combined = np.concatenate(([predicted_results[i]], [actual_prices[i]]))
        combined = combined[:, ~np.isnan(combined).any(axis=0)]
        if len(combined[0]) > 0: # if without this, there will be expection when days < 5
            result.append(mean_squared_error(combined[0], combined[1]))
        else:
            result.append(np.nan)
    for i in range(0, len(predicted_results)):
        combined = np.concatenate(([predicted_results[i]], [actual_prices[i]]))
        combined = combined[:, ~np.isnan(combined).any(axis=0)]
        if len(combined[0]) > 0:  # if without this, there will be expection when days < 5
            result.append(mean_absolute_error(combined[0], combined[1]))
        else:
            result.append(np.nan)
    columns = ['mse%s' % (i + 1) for i in range(len(predicted_results))]
    columns.extend(['mae%s' % (i + 1) for i in range(len(predicted_results))])
    df = pd.DataFrame([result], columns=columns)
    df = df.dropna(axis=1, how='all')
    df.to_csv(_get_root_dir_name(ticker) + '%s_errors.csv' % ticker)


# def get_confidence_interval(ticker, predicted_prices, actual_prices, dates=None):
#     assert len(predicted_prices) == len(actual_prices)
#     days_ahead = len(predicted_prices)
#     print("\nConfidence intervals:")
#     corrects=[]
#     for i in range(days_ahead):
#         corrects.append(0)
#     for step in range(days_ahead):
#         for i in range(len(actual_prices[step])):
#             actual = actual_prices[step][i]
#             forecast = predicted_prices[step][i]
#             if not actual == np.nan:
#                 if (forecast >= (1 - 0.005) * actual) and (forecast <= (1 + 0.005) * actual):
#                     corrects[step] = corrects[step] + 1
#         if len(predicted_prices[step]) == 0:
#             corrects[step] = 0
#         else:
#             corrects[step] = corrects[step]/len(predicted_prices[step])
#     corrects = np.array(corrects, dtype=float)
#     columns = ['days%s' % (i+1) for i in range(days_ahead)]
#     df = pd.DataFrame([corrects], columns=columns)
#     df.to_csv(_get_root_dir_name(ticker) + '%s_confidence_interval.csv' % ticker)


def use_all_backtest_methods(ticker_name, predicted_prices, actual_prices, start_dates=None, predicted_dates=None):
    '''
    ticker_name: ticker name
    predicted_price : should be [[1_days_ahead_p1,1_days_ahead_p2,1_days_ahead_p3],
                                 [2_days_ahead_p1,2_days_ahead_p2,2_days_ahead_p3]]
    '''
    if _check_list_type(predicted_prices) is pd.Series:
        predicteds = [[] for i in range(len(actual_prices))]
        actuals = [[] for i in range(len(actual_prices))]
        for i in range(len(actual_prices)):
            for j in range(len(actual_prices[i])):
                actuals[i].append(actual_prices[i][j]['quandl_eod_stock_prices_Adj_Close'])

        for i in range(len(predicted_prices)):
            for j in range(len(predicted_prices[i])):
                predicteds[i].append(predicted_prices[i][j]['quandl_eod_stock_prices_Adj_Close'])
    else:
        predicteds = predicted_prices
        actuals = actual_prices

    write_price_pic(ticker_name, predicteds, actuals, start_dates)
    #write_csv(ticker_name, predicteds, actuals, start_dates,predicted_dates)
    calculate_error(ticker_name, predicteds, actuals, start_dates)
    # get_confidence_interval(ticker_name, predicteds, actuals, start_dates)



