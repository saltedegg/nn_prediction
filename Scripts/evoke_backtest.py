from backtest_util import use_all_backtest_methods
from high_level_prediction import backtest

models_and_data = [("LSTM", "0.1", "quandl_eod_stock_prices+marketpsych", "0.1"),
                   ("CNN1", "0.1", "quandl_eod_stock_prices+marketpsych", "0.1"),
                   ("LR", "0.1", "quandl_eod_stock_prices+ibm_watson_alchemy_news", "0.1")]

tickers = ["AAPL"]
horizon = [0, 1]
backtest_len = -3

for ticker in tickers:
    predicted_prices, real_prices = backtest(models_and_data, ticker, horizon, backtest_len)
    use_all_backtest_methods(ticker, predicted_prices, real_prices)
