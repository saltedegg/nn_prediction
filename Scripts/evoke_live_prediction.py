from high_level_prediction import live_prediction

models_and_data = [("MULTI_LSTM", "0.1", "quandl_eod_stock_prices+marketpsych", "0.1")]

tickers = ["AAPL", "EA"]
horizon = [0, 1, 2, 3, 4]

for ticker in tickers:
    live_prediction(models_and_data, ticker, horizon)
