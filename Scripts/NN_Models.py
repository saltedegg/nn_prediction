"""

Watstock Models Module

"""

"""

Imports

"""
import os

# os.environ['KERAS_BACKEND'] = 'theano'  # set Keras to work on theano (GPU mode)
from keras.models import Sequential
from keras.layers import Input, concatenate, regularizers
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.recurrent import GRU
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import MaxPooling1D
from keras.layers.merge import Concatenate
from keras.models import Model
from sklearn.svm import SVR

"""

Functions

"""


def create_model_cnn1(shapes, hparams, dropout):
    """
    Create Single CNN Model

    :param shapes: input layer shape
    :param hparams: list of shapes for each conv filter and dense layer
    :param dropout: dropout param for drop out layer
    :return: CNN model
    """
    #### WIll ADD STATEFUL LSTM FUNCTIONALITY BUT NEEDS BATCH_SIZE SET TO 1 WHICH GIVES ONLINE LEARNING FACILITY
    model = Sequential()
    model.add(Conv1D(filters=hparams[0], kernel_size=5, activation='relu', input_shape=(shapes[1], shapes[0])))
    model.add(MaxPooling1D(2))
    model.add(Conv1D(filters=hparams[1], kernel_size=4, activation='relu'))
    model.add(MaxPooling1D(2))
    model.add(Dropout(dropout))
    model.add(Flatten())
    model.add(Dense(hparams[2], kernel_initializer="glorot_uniform", activation='relu'))
    model.add(Dense(hparams[3], kernel_initializer="glorot_uniform", activation='linear'))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model


def create_model_cnn2(shapes, hparams, dropout, filter_sizes=[4, 5]):
    """
    Create Multiple CNN Model

    :param shapes: input layer shape
    :param hparams: list of shapes for layers
    :param dropout: dropout param for dropout layer
    :param filter_sizes: filter sizes
    :return: CNN model
    """
    X = Input(shape=(shapes[1], shapes[0]))
    gru1 = GRU(hparams[0], return_sequences=True, name='gru1')(X)
    conv_blocks = []
    for size in filter_sizes:
        conv = Conv1D(filters=hparams[0],
                      kernel_size=size,
                      padding="valid",
                      activation="relu",
                      strides=1)(gru1)
        conv = MaxPooling1D(pool_size=2)(conv)
        conv = Flatten()(conv)
        conv_blocks.append(conv)
    z = Concatenate()(conv_blocks) if len(conv_blocks) > 1 else conv_blocks[0]

    z = Dropout(dropout)(z)
    z = Dense(hparams[1], activation="relu")(z)
    model_output = Dense(1, activation="linear")(z)

    model = Model(X, model_output)
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model


def create_model_lstm(shapes, hparams, dropout):
    """
    Create Single LSTM Model

    :param shapes: shape for input layer
    :param hparams: list of shape for layers
    :param dropout: dropout param for dropout layer
    :return: LSTM model
    """
    #### WIll ADD STATEFUL LSTM FUNCTIONALITY BUT NEEDS BATCH_SIZE SET TO 1 WHICH GIVES ONLINE LEARNING FACILITY
    model = Sequential()
    model.add(GRU(hparams[0], input_shape=(shapes[1], shapes[0]), return_sequences=True))
    model.add(Dropout(dropout))

    model.add(GRU(hparams[1], input_shape=(shapes[1], shapes[0]), return_sequences=False))
    model.add(Dropout(dropout))

    model.add(Dense(hparams[2], kernel_initializer="glorot_uniform", activation='relu'))
    model.add(Dense(hparams[3], kernel_initializer="glorot_uniform", activation='linear'))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model


def create_model_linear_regression(shapes):
    model = Sequential()
    model.add(Dense(1, input_dim=shapes[0], activation='linear'))
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model


def create_model_svr():
    model = SVR(verbose=True)
    return model


def crop_multi_lstm_input(input, length_a, length_b):
    return [input[:, :, 0:length_a],
            input[:, :, length_a:length_a + length_b]]


def create_model_multi_lstm(shapes, hparams, hparams2, dropout, social_feature_length, news_feature_length):
    #### WIll ADD STATEFUL LSTM FUNCTIONALITY BUT NEEDS BATCH_SIZE SET TO 1 WHICH GIVES ONLINE LEARNING FACILITY
    # X = Input(shape=(shapes[1], shapes[0],))
    #
    # def crop(a):
    #     return a[:, :, 0:social_feature_length]
    #
    # def crop2(a):
    #     return a[:, :, social_feature_length:social_feature_length + news_feature_length]
    #
    # def output_of_lambda(input_shape):
    #     # print input_shape
    #     return (input_shape[0], input_shape[1], social_feature_length)
    #
    # def output_of_lambda2(input_shape):
    #     return (input_shape[0], input_shape[1], news_feature_length)

    # x1 = Lambda(crop, output_shape=output_of_lambda)(X)
    # x2 = Lambda(crop2, output_shape=output_of_lambda2)(X)

    x1 = Input(shape=(shapes[1], social_feature_length))
    x2 = Input(shape=(shapes[1], news_feature_length))

    # print("x1 keras shape", x1._keras_shape)
    # model.add(TimeDistributed(Dense(neurons[0],activation='tanh'),input_shape=(layers[1], layers[0],)))
    gru1 = GRU(hparams[0], return_sequences=True, name='gru1')(x1)
    gru2 = GRU(hparams2[0], return_sequences=True, name='gru2')(x2)

    drop1 = Dropout(dropout)(gru1)
    drop2 = Dropout(dropout)(gru2)

    gru3 = GRU(hparams[1], return_sequences=False, name='gru3')(drop1)
    gru4 = GRU(hparams2[1], return_sequences=False, name='gru4')(drop2)

    drop3 = Dropout(dropout)(gru3)
    drop4 = Dropout(dropout)(gru4)

    # gru5 = GRU(hparams[2], return_sequences=False, name='gru5')(drop3)
    # gru6 = GRU(hparams2[2], return_sequences=False, name='gru6')(drop4)
    #
    # drop5 = Dropout(dropout)(gru5)
    # drop6 = Dropout(dropout)(gru6)

    dense1 = Dense(hparams[2], kernel_initializer="glorot_uniform", activation='relu', name='dense1',
                   kernel_regularizer=regularizers.l2(0.1), activity_regularizer=regularizers.l1(0.1))(drop3)
    dense2 = Dense(hparams2[2], kernel_initializer="glorot_uniform", activation='relu', name='dense2',
                   kernel_regularizer=regularizers.l2(0.1), activity_regularizer=regularizers.l1(0.1))(drop4)

    merged_features = concatenate([dense1, dense2])  # change here with different method

    outputs = Dense(hparams[3], kernel_initializer="glorot_uniform", activation='linear')(merged_features)

    # model = Model(X, outputs)
    model = Model(inputs=[x1, x2], outputs=outputs)
    model.compile(loss='mse', optimizer='adam', metrics=['mae'])
    model.summary()
    return model
