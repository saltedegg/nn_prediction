"""

Get model config according to model name and horizon


"""

def get(modelname, horizon):

    if modelname == 'CNN1':
        from CNN1 import Config
    elif modelname == 'CNN1_watson':
        from CNN1_watson import Config
    elif modelname == 'CNN2':
        from CNN2 import  Config
    elif modelname == 'LR':
        from LR import Config
    elif modelname == 'LSTM':
        from LSTM import Config
    elif modelname == 'MULTI_LSTM':
        from MULTI_LSTM import Config
    else:
        print "Model name is invalid"
        exit(1)

    config = Config[horizon]
    shape = config['shape']
    params = config['params']
    dropout = config['dropout']
    batch_size = config['batch_size']
    epochs = config['epochs']
    decay = config['decay']

    settings = {'shape': shape, 'params': params, 'dropout': dropout, 'batch_size': batch_size,
                'epochs': epochs, 'decay': decay}

    return settings