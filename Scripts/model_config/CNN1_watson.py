"""


CNN1 model for IBM Watson data source configuration


"""

Config = {
    0:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    1:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    2:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    3:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    4:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    5:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    6:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    7:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    8:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        },

    9:
        {
            'shape': [15, 40, 1],
            'params': [128, 128, 32, 1],
            'dropout': 0.2,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2
        }

}