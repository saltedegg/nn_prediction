"""

Mulitiple LSTM configuration

"""

Config = {
    0:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    1:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    2:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 300,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    3:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 200,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    4:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 200,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    5:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 200,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    6:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 150,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    7:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 100,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    8:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 50,
            'decay': 0.2,
            'length_a': 5,
            'length_b': 5
        },

    9:
        {
            'shape': [66, 60, 1],
            'params': [[128, 64, 32, 1], [64, 32, 32, 1]],
            'dropout': 0.4,
            'batch_size': 100,
            'epochs': 50,
            'decay': 0.2,
            'length_a': 6,
            'length_b': 6
        }

}