import os

from sklearn.decomposition import PCA
from sklearn.preprocessing import MaxAbsScaler
import pandas as pd
import pickle


# this function takes the df as a parameter and is the starting point for all data preprocessing
def preprocess(df, get_new_models, datasource):
    if datasource == 'quandl_eod_stock_prices+marketpsych':
        df = preprocess_marketpsych(df, get_new_models, datasource)
    if datasource == 'quandl_eod_stock_prices+ibm_watson_alchemy_news':
        df = preprocess_watson(df, get_new_models, datasource)
    return df


def preprocess_marketpsych(df, get_new_models, datasource):
    index = df.index
    range_relation = pd.DataFrame(df[['range_relation']], index=index)
    price_relation = pd.DataFrame(df[['price_relation']], index=index)
    droplist = ['range_relation', 'price_relation']
    sentiment_data = df.drop(droplist, axis=1)

    pca_model = get_pca_model(get_new_models, datasource, sentiment_data)
    sentiment_data = transform_sentiment_features_to_pc(sentiment_data, pca_model)
    if get_new_models:
        sentiment_data = remove_outliers(sentiment_data)
    scaler = get_scaler(get_new_models, datasource, sentiment_data)
    sentiment_data = get_scaled_features(sentiment_data, scaler)
    df = pd.concat([pd.DataFrame(sentiment_data, index=index), range_relation, price_relation], axis=1)
    return df

def preprocess_watson(df, get_new_models, datasource):
    index = df.index
    range_relation = pd.DataFrame(df[['range_relation']], index=index)
    price_relation = pd.DataFrame(df[['price_relation']], index=index)
    df_watson = df[[col for col in df if col.startswith('ibm_watson')]]
    df_watson = df_watson.drop(['ibm_watson_alchemy_news_titles'], axis=1)
    scaler = get_scaler(get_new_models, datasource, df_watson)
    df_watson = get_scaled_features(df_watson, scaler)
    df = pd.concat([pd.DataFrame(df_watson, index=index), range_relation, price_relation], axis=1)
    return df


def generate_pca_model_from_sentiment_features(features, n_components):
    """
            Convert sentiment features into principal components

            :param features: features to generate pc from
            :param n_components: number of principal components
            :return: pca model
            """
    pca = PCA(n_components=n_components)
    pca.fit(features)
    return pca


def transform_sentiment_features_to_pc(features, pca_model):
    transformed_features = pca_model.transform(features)
    return transformed_features


def convert_pct_to_price(pcts, last_price):
    """
    Convert percentage changes into close price

    :param pcts: percentage changes
    :param last_price: percentage change target price
    :return: de-normalized prediction result
    """
    result = []
    for p in pcts:
        result.append(last_price + last_price * p)
    return result


def get_pca_model(generate_new_pca_model, datasource, df=None):
    if not os.path.exists('pca'):
        os.makedirs('pca')
    pcaname = 'pca/pca_%s.sav' % datasource
    if generate_new_pca_model:
        pca_model = generate_pca_model_from_sentiment_features(df, n_components=10)
        with open(pcaname, 'wb') as output:
            pickle.dump(pca_model, output)
    else:
        with open(pcaname, 'rb') as input:
            pca_model = pickle.load(input)
    return pca_model


def generate_scaler_from_dataframe(df):
    scaler = MaxAbsScaler().fit(df)
    return scaler


def get_scaler(generate_new_scaler, datasource, df=None):
    if not os.path.exists('scaler'):
        os.makedirs('scaler')
    scalername = 'scaler/scaler_%s.sav' %datasource
    if generate_new_scaler:
        scaler = generate_scaler_from_dataframe(df)
        with open(scalername, 'wb') as output:
            pickle.dump(scaler, output)
    else:
        with open(scalername, 'rb') as input:
            scaler = pickle.load(input)
    return scaler


def get_scaled_features(df, scaler):
    df = scaler.transform(df)
    return df


def remove_outliers(df):
    df = pd.DataFrame(df)
    df = df.clip(-1000, 1000, axis=1)
    return df
