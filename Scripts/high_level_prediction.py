from watstocksdk import Watstock

from low_level_models import train_low_level_models, get_model, get_input_for_previous_day
import numpy as np
import tensorflow as tf
from keras import backend as K
import datetime
from holidays import UnitedStates
from bdateutil import relativedelta

watstock = Watstock(access_token="aWnkBW0teY")
SEPERATE_WRITE = False
WRITE_TO_SERVER = False


# day=-1 to predict live price, lower values for backtesting
def predict_high_level_price(models_and_data, ticker, horizons, day=-1, backtest=False):
    train_low_level_models(models_and_data, ticker, horizons, backtest, day)
    predictions = []
    real_prices = []
    for horizon in horizons:
        daily_predictions = []
        prediction_date = 0
        for modelname, modelversion, datasource, version in models_and_data:
            K.clear_session()
            session = tf.Session()
            K.set_session(session)
            data, actual_change, last_price, date = get_input_for_previous_day(datasource, version, ticker, modelname, horizon, day)
            prediction_date = date
            model = get_model(modelname, ticker, datasource, horizon)
            prediction = model.predict(data)
            price_prediction = last_price + prediction * last_price
            actual_price = last_price + actual_change * last_price
            print('date:', date)
            print('last_price:', last_price)
            print('horizon:', horizon)
            print('low_level_prediction:', price_prediction)
            print('actual_price:', actual_price)

            if SEPERATE_WRITE:
                write_prediction_to_server(modelname, modelversion, ticker, date, horizon, price_prediction)
            daily_predictions.append(price_prediction)
        # In the future the way of combining the predictions might still change, bagging is a very simple
        # approach that should still lead to good results at low cost once enough datasources are available
        # It's also very suitable once the clients are able to chose a combination of datasources via microtransactions
        # More complex approaches would then lead to feeding a model for every possible combination of datasources
        # That would be way to inefficient
        average_prediction = np.mean(daily_predictions)
        predictions.append(average_prediction)
        real_prices.append(actual_price)
        write_prediction_to_server('Stacked', '0.1', ticker, prediction_date, horizon, average_prediction)
    return predictions, real_prices

def backtest(models_and_data, ticker, horizons, day):
    predicted_prices = []
    real_prices = []
    for i in range(day, -1):
        pred, real = predict_high_level_price(models_and_data, ticker, horizons, day=i, backtest=True)
        predicted_prices.append(pred)
        real_prices.append(real)
    #transformtion required for backtest utility
    predicted_prices = [[x[h] for x in predicted_prices] for h in horizons]
    real_prices = [[x[h] for x in real_prices] for h in horizons]
    print('predicted_prices:', predicted_prices, 'real_prices', real_prices)
    return predicted_prices, real_prices


def live_prediction(models_and_data, ticker, horizons):
    predict_high_level_price(models_and_data, ticker, horizons)


def write_prediction_to_server(model_name, model_version, ticker, date, horizon, prediction):
    # Calculate prediciton date
    date = date + datetime.timedelta(hours=21, minutes=0, seconds=0)
    prediction_date = date + relativedelta(bdays=+(horizon + 1), holidays=UnitedStates())

    # Calculate horizon value
    horizon_value = (horizon + 1) * 86400

    # Logs
    print('ticker:', ticker)
    print('prediction_date:', prediction_date)
    print('horizon_value', horizon_value)
    print('prediction', prediction)

    # Write to server
    if WRITE_TO_SERVER:
        watstock.write.prediction(model=model_name,
                                  version=model_version,
                                  ticker=ticker,
                                  prediction=prediction,
                                  prediction_date=prediction_date,
                                  horizon=horizon_value
                                  )
