import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from watstocksdk import Watstock
import pickle
import numpy as np

from NN_Models import *
from data_loading import get_data_starting_at_date, get_df_X, get_df_y, make_X_3d, \
    convert_multiindex_to_3d_array, get_data
from data_preprocessing import preprocess
from feature_engineering import engineer_features
import model_config

watstock = Watstock(access_token="aWnkBW0teY")
base_path = 'live/'


def train_low_level_models(models_and_data, ticker, horizons, backtest, day):
    if backtest:
        global base_path
        base_path = 'backtest/'
    models = []
    for horizon in horizons:
        horizon_models = []
        for modelname, modelversion, datasource, version in models_and_data:
            K.clear_session()
            session = tf.Session()
            K.set_session(session)
            print(modelname, horizon)
            data = get_data(datasource, version, ticker, day)
            model = get_model(modelname, ticker, datasource, horizon)
            model = feed_model(modelname, model, data, ticker, horizon, datasource)
            horizon_models.append(model)
        models.append(horizon_models)
    print(models, 'trained successfully')


'''TODO: Right now we download the complete history every day
 the sdk needs a way to query just for data starting at a specified date.
 We can than save the last trained date and query just for data starting at that point'''


def get_model(modelname, ticker, datasource, horizon):
    backtest_model_path = base_path + 'models/%s_%s/backtest_model/' % (modelname, datasource)
    if not os.path.exists(backtest_model_path):
        os.makedirs(backtest_model_path)
    modelpath = backtest_model_path + '%s_%s_%s_%sday_ahead_Model.h5' % (ticker, modelname, datasource, horizon + 1)
    if os.path.isfile(modelpath):
        model = load_model(
            modelpath)
        model.compile(loss='mse', optimizer='adam', metrics=['mae'])
        print(modelpath, 'loaded successfully')

    else:

        settings = get_model_settings(modelname, horizon)

        if modelname == 'CNN1':
            model = create_model_cnn1(settings['shape'], settings['params'],
                                      settings['dropout'])  # params, dropout, shape
        if modelname == 'CNN1_watson':
            model = create_model_cnn1(settings['shape'], settings['params'],
                                      settings['dropout'])  # params, dropout, shape
        if modelname == 'CNN2':
            model = create_model_cnn2(settings['shape'], settings['params'],
                                      settings['dropout'])  # params, dropout, shape
        if modelname == 'LSTM':
            model = create_model_lstm(settings['shape'], settings['params'],
                                      settings['dropout'])  # params, dropout, shape
        if modelname == 'LR':
            model = create_model_linear_regression(settings['shape'])  # shape
        if modelname == 'MULTI_LSTM':
            model = create_model_multi_lstm(settings['shape'], settings['params'][0], settings['params'][1],
                                      settings['dropout'], settings['length_a'], settings['length_b'])
        print(modelname, 'created successfully')
    return model


def feed_model(modelname, model, df, ticker, horizon, datasource):
    backtest_model_path = base_path + 'models/%s_%s/backtest_model/' % (modelname, datasource)
    modelpath = backtest_model_path + '%s_%s_%s_%sday_ahead_Model.h5' % (ticker, modelname, datasource, horizon + 1)
    timestamps = df.index.tolist()
    current_date = timestamps[-1]
    last_trained_date = get_last_trained_date_for_model(ticker, modelname, datasource, horizon)
    if not last_trained_date or current_date > last_trained_date:
        df = engineer_features(df, datasource)
        train_X = get_df_X(df)
        train_y = get_df_y(df, horizon)
        # get new models if no previous training
        train_X = preprocess(train_X, (not last_trained_date), datasource)
        seq_len = get_model_seq_len(modelname, horizon)
        batch_size = get_batch_size(modelname, horizon)
        epochs = get_epochs_for_model_and_horizon(modelname, horizon)
        if (seq_len > 1):
            train_X = make_X_3d(train_X, seq_len)
            train_X = get_data_starting_at_date(train_X, last_trained_date)
            train_y = train_y.loc[train_X.index.get_level_values(0).unique()]
            # save latest date to know where to continue next training
            train_X = convert_multiindex_to_3d_array(train_X)
            if str.startswith(modelname, 'MULTI_LSTM'):
                length_a = get_length_a(modelname, horizon)
                length_b = get_length_b(modelname, horizon)
                train_X = crop_multi_lstm_input(train_X, length_a, length_b)
            train_y = np.array(train_y)
            # retrain
            if not last_trained_date:
                model.fit(train_X, train_y, epochs=epochs, batch_size=batch_size, verbose=1)
            else:
                model.fit(train_X, train_y, epochs=1, batch_size=batch_size, verbose=1)
        else:
            train_X = get_data_starting_at_date(train_X, last_trained_date)
            train_y = get_data_starting_at_date(train_y, last_trained_date)
            # save latest date to know where to continue next training
            train_X = np.array(train_X)
            train_y = np.array(train_y)
            # retrain
            if not last_trained_date:
                model.fit(train_X, train_y, epochs=get_epochs_for_model_and_horizon(modelname, horizon), verbose=1)
            else:
                model.fit(train_X, train_y, epochs=1, verbose=1)

        model.save(modelpath)
        save_latest_date_locally(df, ticker, modelname, datasource, horizon)
    else:
        print('Model already fully trained')
    return modelpath


def save_latest_date_locally(df, ticker, modelname, datasource, horizon):
    data_folder = base_path + 'data'
    data_path = data_folder + '/%s_%s_%s_%s' % (ticker, modelname, datasource, horizon)
    timestamps = df.index.tolist()
    date = timestamps[-1]
    pickle.dump(date, open(data_path, "wb"))


def get_last_trained_date_for_model(ticker, modelname, datasource, horizon):
    data_folder = base_path + 'data'
    data_path = data_folder + '/%s_%s_%s_%s' % (ticker, modelname, datasource, horizon)
    if not os.path.exists(data_folder):
        os.mkdir(data_folder)
    # start training from day 0 if we never downloaded data
    if not os.path.isfile(data_path):
        return None
    # else assume that the model is trained on the data has been downloaded already and resume
    # training after that
    else:
        date = pickle.load(open(data_path, "rb"))
    return date


def get_input_for_previous_day(datasource, version, ticker, modelname, horizon, day=-1):
    df = get_data(datasource, version, ticker, day)
    timestamps = df.index.tolist()
    date = timestamps[-1]
    last_price = df.loc[date, 'quandl_eod_stock_prices_Close']
    df = engineer_features(df, datasource)
    test_X = get_df_X(df)
    test_X = preprocess(test_X, False, datasource)
    test_y = get_df_y(df, horizon).loc[date]
    seq_len = get_model_seq_len(modelname, horizon)
    if (seq_len > 1):
        test_X = make_X_3d(test_X, seq_len)
        test_X = test_X.loc[date:date]
        test_X = convert_multiindex_to_3d_array(test_X)
        if str.startswith(modelname, 'MULTI_LSTM'):
            length_a = get_length_a(modelname, horizon)
            length_b = get_length_b(modelname, horizon)
            test_X = crop_multi_lstm_input(test_X, length_a, length_b)
    else:
        test_X = test_X.loc[date]
        test_X = np.array(test_X).reshape(1, -1)

    return test_X, test_y, last_price, date


def get_model_settings(modelname, horizon):
    settings = model_config.get(modelname, horizon)
    return settings


def get_model_seq_len(modelname, horizon):
    shape = get_model_settings(modelname, horizon)['shape']
    seq_len = shape[1]
    return seq_len


def get_epochs_for_model_and_horizon(modelname, horizon):
    settings = get_model_settings(modelname, horizon)
    epochs = settings['epochs']
    return epochs


def get_batch_size(modelname, horizon):
    settings = get_model_settings(modelname, horizon)
    batch_size = settings['batch_size']
    return batch_size

def get_length_a(modelname, horizon):
    settings = get_model_settings(modelname, horizon)
    length_a = settings['length_a']
    return length_a

def get_length_b(modelname, horizon):
    settings = get_model_settings(modelname, horizon)
    length_b = settings['length_b']
    return length_b
