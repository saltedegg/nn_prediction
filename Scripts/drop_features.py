"""

Drop features from dataframe


"""


"""

Imports

"""
import pandas as pd


"""

Functions

"""
def get_close_features():
    """
    Get adjusted close price features

    :return: close price column name
    """
    return ['quandl_eod_stock_prices_Adj_Close']


def get_drop_columns():
    """
    Get the columns need to be droped

    :return: columns need to be droped
    """
    result = []
    for i in range(1, 6):
            result.extend(['open_pct_%d' % i, 'high_pct_%d' % i, 'low_pct_%d' % i, 'vol_pct_%d' % i,
                           'close_pct_%d' % i])
    return result


def get_price_features():
    """
    Get actual price columns

    :return: actual price column names
    """
    return ['quandl_eod_stock_prices_Adj_Open',
            'quandl_eod_stock_prices_Adj_High',
            'quandl_eod_stock_prices_Adj_Low',
            'quandl_eod_stock_prices_Adj_Close']


def just_keep_quandl(df):
    """
    Just keep actual price columns in dataframe

    :param df: input dataframe
    :return: selected dataframe
    """
    return df[['quandl_eod_stock_prices_Adj_Open', 'quandl_eod_stock_prices_Adj_High', 'quandl_eod_stock_prices_Adj_Low',
             'quandl_eod_stock_prices_Adj_Volume', 'quandl_eod_stock_prices_Adj_Close']]


def make_pct_features(df):
    """
    Make percentage change features

    :param df: original dataframe
    :return: dataframe with percentage change features
    """
    df_pct1 = df.pct_change(1).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_1',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_1',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_1',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_1',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_1'})

    df_pct2 = df.pct_change(2).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_2',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_2',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_2',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_2',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_2'})

    df_pct3 = df.pct_change(3).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_3',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_3',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_3',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_3',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_3'})

    df_pct4 = df.pct_change(4).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_4',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_4',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_4',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_4',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_4'})

    df_pct5 = df.pct_change(5).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_5',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_5',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_5',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_5',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_5'})

    new_df = pd.concat([df_pct1, df_pct2, df_pct3, df_pct4, df_pct5], axis=1, join_axes=[df_pct1.index])
    return new_df