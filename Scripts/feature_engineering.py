import pandas as pd
import numpy as np


# There exists one function per datasource

def engineer_features(df, datasource):
    if datasource == 'quandl_eod_stock_prices+marketpsych':
        df = engineer_features_marketpsych_eod(df)
    if datasource == 'quandl_eod_stock_prices+ibm_watson_alchemy_news':
        df = engineer_features_watson_eod(df)
    return df


def engineer_features_marketpsych_eod(df):
    df_market = df[[col for col in df if col.startswith('marketpsych_')]]
    df = df[['quandl_eod_stock_prices_Adj_Open', 'quandl_eod_stock_prices_Adj_High', 'quandl_eod_stock_prices_Adj_Low',
             'quandl_eod_stock_prices_Adj_Volume', 'quandl_eod_stock_prices_Adj_Close']]
    range_relation = generate_atr_relation(df.copy())
    price_relation = generate_price_relation(df.copy())
    atr = generate_atr(df.copy(), 20)  # todo: 20 period, test period
    df_pct1, df_pct2, df_pct3, df_pct4, df_pct5 = generate_pct_from_df(df.copy())
    df = pd.concat([df_pct1, df_pct2, df_pct3, df_pct4, df_pct5, df_market, range_relation, price_relation, atr],
                   axis=1, join_axes=[df_pct1.index])
    return df

def engineer_features_watson_eod(df):
    df_watson = df[[col for col in df if col.startswith('ibm_watson')]]
    df = df[['quandl_eod_stock_prices_Adj_Open', 'quandl_eod_stock_prices_Adj_High', 'quandl_eod_stock_prices_Adj_Low',
             'quandl_eod_stock_prices_Adj_Volume', 'quandl_eod_stock_prices_Adj_Close']]
    range_relation = generate_atr_relation(df.copy())
    price_relation = generate_price_relation(df.copy())
    atr = generate_atr(df.copy(), 20)  # todo: 20 period, test period
    df_pct1, df_pct2, df_pct3, df_pct4, df_pct5 = generate_pct_from_df(df.copy())
    df = pd.concat([df_pct1, df_pct2, df_pct3, df_pct4, df_pct5, df_watson, range_relation, price_relation, atr],
                   axis=1, join_axes=[df_pct1.index])
    return df

def generate_pct_from_df(df):
    df_pct1 = df.pct_change(1).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_1',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_1',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_1',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_1',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_1'})

    df_pct2 = df.pct_change(2).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_2',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_2',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_2',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_2',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_2'})

    df_pct3 = df.pct_change(3).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_3',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_3',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_3',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_3',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_3'})

    df_pct4 = df.pct_change(4).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_4',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_4',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_4',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_4',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_4'})

    df_pct5 = df.pct_change(5).fillna(0.0) \
        .rename(columns={'quandl_eod_stock_prices_Adj_Open': 'open_pct_5',
                         'quandl_eod_stock_prices_Adj_High': 'high_pct_5',
                         'quandl_eod_stock_prices_Adj_Low': 'low_pct_5',
                         'quandl_eod_stock_prices_Adj_Volume': 'vol_pct_5',
                         'quandl_eod_stock_prices_Adj_Close': 'close_pct_5'})

    return df_pct1, df_pct2, df_pct3, df_pct4, df_pct5


# this feature represents a factor that shows how the short term atr relates to the longer term atr
def generate_price_relation(df):
    # can't calculate so assume most neutral value
    first_20_prices = [0.5 for _ in range(20)]
    high = np.array([get_high_from_range(df, x - 20, x + 1) for x in range(20, len(df))])
    low = np.array([get_low_from_range(df, x - 20, x + 1) for x in range(20, len(df))])
    price = df['quandl_eod_stock_prices_Adj_Close'][20:]
    df['price_relation'] = np.append(first_20_prices, np.divide((price - low), (high - low)))
    # get price in relation to low and high / range between 0 and 1
    return df['price_relation']


def get_high_from_range(df, n, m):
    return np.amax([df['quandl_eod_stock_prices_Adj_Close'][x] for x in range(n, m)])


def get_low_from_range(df, n, m):
    return np.amin([df['quandl_eod_stock_prices_Adj_Close'][x] for x in range(n, m)])


def get_atr_from_range(df, n, m):
    return np.mean([df['range'][x] for x in range(n, m)])


# this feature represents a factor that shows where we are in the current range
def generate_atr_relation(df):
    # can't calculate as there is no more previous data so assume a neutral relation of 1
    df['range'] = df['quandl_eod_stock_prices_Adj_High'] - df['quandl_eod_stock_prices_Adj_Low']
    first_20_elements = np.ones(20)
    slow_atr = [get_atr_from_range(df, x - 20, x) for x in range(20, len(df))]
    fast_atr = [get_atr_from_range(df, x - 2, x) for x in range(20, len(df))]
    atr_relation = np.divide(fast_atr, slow_atr)
    df['range_relation'] = np.append(first_20_elements, atr_relation)
    return df['range_relation']


def generate_atr(df, period):
    df['range'] = df['quandl_eod_stock_prices_Adj_High'] - df['quandl_eod_stock_prices_Adj_Low']
    first_elements = np.ones(period)
    df['atr'] = np.append(first_elements, [get_atr_from_range(df, x - period, x) for x in range(period, len(df))])
    return df['atr']
